# waves
c++ game with OpenGL.

# Build
```console
mkdir build/ &&
cmake -S . -B ./build/ &&
cmake --build ./build/ 
```

# Dependencies
 - [glew](https://github.com/nigels-com/glew)
 - [glfw](https://github.com/glfw/glfw)
 - [glm](https://github.com/g-truc/glm)
 - [stbi](https://github.com/nothings/stb)

